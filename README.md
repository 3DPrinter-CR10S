# CR10S with BLTouch (3.1) and Marlin 2.0.x

## Why try 2.0?
Installation of BLTouch has been done following instructions that shipped with the kit (Creality kit) and all is working using the precompiled version of Marlin (1.1.6) that Creality provides.
ABL is working as well.

It only covers the 3x3 point ABL and I would like to experiment with more probe points but most important is to remove support for "Power loss detection" which writes each OP onto the SD-card...

## Wiring & installation
Premise: Since it is working in precompiled firmware provided by Creality, I assume the wiring is sound?

## Troubleshooting

### 2021-05-20
**Marlin version: 2.0.8.1 (using CR10S example config as base)**

- Flashed 2.0.8.1 (using CR10S V1 example config as base and applied changes for enabling BLTouch)
- Specified Z-probe offset to -2.58 (which worked and was used in 1.1.6 blob provided by Creality with BLTouch support)

**Testing of endstops**
- M119 while triggering endstop for X -> Triggered
- M119 while not triggering endstop for X -> open
- M119 while triggering endstop for Y -> Triggered
- M119 while not triggering endstop for Y -> open
- Deploying BLTouch probe from within BLTouch control panel.
- M119 while probe is deployed: Z -> open
- Stowing probe (with a paper)
- M119 while probe is stowed: Z -> Triggered

**Testing homing of axes**
- G28 X -> Homes X without any problem (issued multiple times in a row with correct outcome)
- G28 Y -> Homes Y without any problem (issued multiple times in a row with correct outcome)
- G28 Z -> Moves to safe homing place, deploys probe and then moves up a tiny bit and then down a tiny but (all in mid air, say 5cm above bed). Probe never touches the bed.

**Note**: If i ONLY do homing of X and Y it works in arbitrary order, but as soon I've tried to home Z and then issuing G28 X or Y the printer halts.

Enabled PINS_DEBUGGING and issued the following G-CODE command: M43 S
Output:
    >>> M43 S
    SENDING:M43 S
    Servo probe test
    . using index:  0, deploy angle: 10, stow angle:   90
    . Probe Z_MIN_PIN: 18
    . Z_MIN_ENDSTOP_INVERTING: false
    . Check for BLTOUCH
    = BLTouch Classic 1.2, 1.3, Smart 1.0, 2.0, 2.2, 3.0, 3.1 detected.
    ** Please trigger probe within 30 sec **
    . Pulse width (+/- 4ms): 0
    FAIL: Noise detected - please re-run test
    ok P15 B3

Ran the test multiple times and the probe was deployed and stowed (only once) during the test. May this be connected to me running some extensions on the cables perhaps?


### 2020-02-22
**Marlin version: 2.0.3 (using CR10S example config as base)**
Enabled PINS_DEBUGGING and ran M43 S which gave the following:
    Servo probe test
    - using index: 0, deploy angle: 10, stow angle: 90
    - Probe Z_MIN_PIN: 18
    - Z_MIN_ENDSTOP_INVERTING: false
    - Check for BLTOUCH
    = BLTOUCH Classic 1.2, 1.3, Smart 1.0, 2.0, 2.2, 3.0, 3.1 detected
    ** Please trigger probe within 30 sec **
    . Pulse width: 30ms or more
    = BLTOUCH V3.1 detected.
    ok P15 B3

See thread at reddit.com: https://www.reddit.com/r/CR10/comments/f7h4cn/cr10s_with_bltouch_and_marlin_20x


